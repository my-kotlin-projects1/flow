import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.random.Random

fun main() {
    runBlocking {
        launch {
            intermediateFlow.collect{
                println(it)
            }
            distinctFlow
                .distinctUntilChanged()
                .collect{
                println(it)
            }
        }
    }
}

val intermediateFlow = (0..10)
    .asFlow()
//    .map { it * it }
    .square()
    .filter { it % 2 == 0 }
    .take(3)

val distinctFlow = flow {
    repeat(10){
        emit(Random.nextBoolean())
        delay(500)
    }
}

fun Flow<Int>.square(): Flow<Int> = transform {
    value -> return@transform emit(value * value)
}
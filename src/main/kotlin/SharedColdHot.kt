import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlin.random.Random

fun main() {
    val numbers = Numbers() // горячий, только создан вот здесь, а уже эмитит.
    runBlocking {
        launch {
            delay(2000) // коллектор коллектит с задержкой, эмиттеру пофиг.
            numbers.sharedFlow.collect { println(it) }
        }
        launch {
            delay(4000) // коллектор коллектит с задержкой, эмиттеру пофиг.
            numbers.sharedFlow.collect { println("Second collector - $it") }
        }
    }
}

class Numbers {
    private val scope = CoroutineScope(Job() + Dispatchers.Default)
    private val _sharedFlow = MutableSharedFlow<Int>(replay = 2)
    val sharedFlow = _sharedFlow.asSharedFlow()

    init {
        scope.launch {
            for (i in 0..10){
                _sharedFlow.emit(i)
                delay(500)
            }
        }
    }
}

object GeneratorShared2 {
    private val scope = CoroutineScope(Job() + Dispatchers.Default)
    private val _sharedFlow = MutableSharedFlow<Int>()
    val sharedFlow = _sharedFlow.asSharedFlow()

    init {
        scope.launch {
            repeat(10){
                _sharedFlow.emit(Random.nextInt(97, 102))
                delay(500)
            }
            _sharedFlow.emit(100)
        }
    }
}